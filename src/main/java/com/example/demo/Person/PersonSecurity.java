package com.example.demo.Person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
public class PersonSecurity extends WebSecurityConfigurerAdapter{
	
	private final PasswordEncoder passwordEncoder;
	
@Autowired	
 public PersonSecurity(PasswordEncoder passwordEncoder) {
		 
		this.passwordEncoder = passwordEncoder;
	}

//First Step
//Basic Auth
	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http
		
		.authorizeRequests()
		.antMatchers("/","index" ,"/getName","/api/signup","/api/*")
		.permitAll()
		.anyRequest()
		.authenticated()
		.and()
		.httpBasic();
	
	
	}
	
	//Second Step
	//In Memory User Details Manager	
	@Override
	@Bean
	protected UserDetailsService userDetailsService() {

		UserDetails user=User.builder()
				.username("dileep")
				.password(passwordEncoder.encode("password"))
				.roles("employee")
				.build();
		
		UserDetails admin=User.builder()
				.username("ravi")
				.password(passwordEncoder.encode("password"))
				.roles("admin")
				.build();
		return new InMemoryUserDetailsManager(user,admin);
	}
}
