package com.example.demo.Person;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class PersonController {

	@Autowired
	PersonRepository personRepository;
	@PostMapping("/signup")
	public String signup(@RequestBody PersonDTO p) {
		Person person = new Person();
		person.setpName(p.pname);
		person.setEmail(p.pmail);
		person.setPassword(p.ppassword);
		personRepository.save(person);
		System.out.println(person);
		return "Success";
	}
	
	@GetMapping("/signin")
	@CrossOrigin(origins = "http://localhost:4200")
public String signIn( ) {
		System.out.println("signin");
	return "signin Successfully";
	
}
	@GetMapping("/employees")
	public List<Person> getAllEmployees() {
		return personRepository.findAll();
	}
	
	@PutMapping("/update/{name}")
	public String  update(@PathVariable(value="name") String Email,@RequestBody Person p) {
		
	p.setEmail(Email);
	personRepository.save(p);
	return "update Successfully";
	}
	
	
}
